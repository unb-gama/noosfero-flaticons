Portal FGA
================
Flaticons para o Portal FGA.

Instruções de uso
=================

1. Na pasta /noosfero/public/designs/icons:
```
cd ~/noosfero/public/designs/icons/
```

2. Clone o repositório alterando o nome da pasta com o seguinte comando:
```
git clone https://gitlab.com/unb-gama/noosfero-flaticons.git flaticons
```

Fonte 
=====
Novos ícones podem ser obtidos pelo seguinte link: 

http://flaticons.net/
